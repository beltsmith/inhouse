package inhouseleague

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/beltsmith/golio"
	api "github.com/beltsmith/golio/api"

	"os"
)

// Client wraps golio.Client providing friendlier access methods
type Client struct {
	*golio.Client
}

func SetupClient() *Client {
	client := golio.NewClient(api.RegionNorthAmerica, os.Getenv("RIOT_KEY"),
		http.DefaultClient, log.StandardLogger())
	return &Client{ client }
}
