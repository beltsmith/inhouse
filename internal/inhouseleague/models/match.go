package models

import (
	"github.com/jinzhu/gorm"
)

type Match struct {
	gorm.Model
	PlayerID uint   `json:"player_id"`
	RemoteID int    `json:"remote_id" gorm:"type:BigInt"`
	Win      string `json:"win"`
	Champion string `json:"champion"`
	Queue    int    `json:"queue"`
	Spell1   string `json:"spell1"`
	Spell2   string `json:"spell2"`
}
