package models

type SummonerSpellList struct {
	Spells map[string]*SummonerSpell `json:"data"`
}
