package inhouseleague

// Service is a composite type providing both API and DB access
type Service struct {
	Api *Client
	DB  *DB
}

func SetupService(environment string) *Service {
	return &Service{
		Api: SetupClient(),
		DB:  SetupDB(environment),
	}
}
